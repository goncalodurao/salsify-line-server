<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\LineController;

Route::get('lines/{line}', [LineController::class, 'show']);
