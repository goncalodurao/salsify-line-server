<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Exception;

class FileProcessService
{
    protected $lineConfig = [];

    function __construct()
    {
        $this->lineConfig = config('file');
    }

    /**
     * @param string $filename
     * @return bool
     * @throws Exception
     */
    public function process($filename)
    {
        if (empty($filename)) {
            throw new Exception('Empty filename');
        }

        if (!file_exists($filename)) {
            throw new Exception('File does not exist');
        }

        $fileHandle = @fopen($filename, "r");
        if (!$fileHandle) {
            throw new Exception('Could not open the file');
        }

        $this->reset();

        // First approach - read line, store line
        if ($this->lineConfig['process_approach'] == 1)
            $this->firstApproachToReadFile($filename, $fileHandle);

        // Second approach - read line, put in array, store array content
        if ($this->lineConfig['process_approach'] == 2)
            $this->secondApproachToReadFile($filename, $fileHandle);

        // Third approach - read block, put in array, store array content
        if ($this->lineConfig['process_approach'] == 3)
            $this->thirdApproachToReadFile($filename, $fileHandle);

        if (!feof($fileHandle)) {
            fclose($fileHandle);
            throw new Exception('Unable to find end of file');
        }
        fclose($fileHandle);

        $this->logMemoryUsage();

        return true;
    }

    /**
     * Clears file related cache, chunks and env/configuration params
     */
    protected function reset()
    {
        config(
            [
                'file.is_loaded' => false,
                'file.line_count' => 0
            ]
        );

        Storage::disk('local')->deleteDirectory($this->lineConfig['chunk_dir']);

        Storage::disk('local')->makeDirectory($this->lineConfig['chunk_dir']);

        // clear cache for info and lines
        Cache::flush();

    }

    /**
     * Read line by line from the file 
     * Append the line to the active chunk
     * Change active chunk when chunk limit reached
     * 
     * @param string $filename
     * @param $fileHandle
     */
    protected function firstApproachToReadFile($filename, $fileHandle)
    {
        $startProcessTime = microtime(true);

        $lineBuffer = '';
        $totalLines = 0;
        $linesForChunk = 0;
        $chunkIndex = 1;
        $chunkPathBase = $this->lineConfig['chunk_dir'].$this->lineConfig['chunk_name_template'];
        $chunkPath = sprintf($chunkPathBase, $chunkIndex);

        while (($lineBuffer = fgets($fileHandle)) !== false) {
            ++$totalLines;
            ++$linesForChunk;
            Storage::disk('local')->append($chunkPath, $lineBuffer, null);
            if ($linesForChunk == $this->lineConfig['chunk_size']) {
                ++$chunkIndex;
                $linesForChunk = 0;
                $chunkPath = sprintf($chunkPathBase, $chunkIndex);
            }
        }

        $info = json_encode([
            'is_loaded' => true,
            'line_count' => $totalLines,
            'chunk_count' => $chunkIndex
        ]); 
        Storage::disk('local')->append($this->lineConfig['chunk_dir'].$this->lineConfig['info_name'], $info);
        Cache::forever($this->lineConfig['info_cache_key'], $info); // this will only expire on new file process

        Log::info(sprintf("Processed file %s, creating %d chunks (of max %d lines) for a total of %d lines, in %s seconds", 
            $filename, 
            $chunkIndex, 
            $this->lineConfig['chunk_size'], 
            $totalLines,
            (microtime(true) - $startProcessTime)
        ));
    }

    /**
     * Read line by line from the file 
     * Push the line to auxiliary array
     * Write imploded array content to active chunk and change active chunk limit reached
     * Write remainder after all the file has been read
     * 
     * @param string $filename
     * @param $fileHandle
     */
    protected function secondApproachToReadFile($filename, $fileHandle)
    {
        $startProcessTime = microtime(true);

        $lineBuffer = '';
        $totalLines = 0;
        $linesForChunk = 0;
        $chunkIndex = 1;
        $chunkPathBase = $this->lineConfig['chunk_dir'].$this->lineConfig['chunk_name_template'];
        $chunkPath = sprintf($chunkPathBase, $chunkIndex);

        $linesArray = [];
        while (($lineBuffer = fgets($fileHandle)) !== false) {
            ++$totalLines;
            ++$linesForChunk;
            $linesArray[] = $lineBuffer;
            if ($linesForChunk == $this->lineConfig['chunk_size']) {
                Storage::disk('local')->put($chunkPath, implode('', $linesArray));
                ++$chunkIndex;
                $linesForChunk = 0;
                $linesArray = [];
                $chunkPath = sprintf($chunkPathBase, $chunkIndex);
            }
        }
        if (count($linesArray)) {
            Storage::disk('local')->put($chunkPath, implode('', $linesArray));
        }

        $info = json_encode([
            'is_loaded' => true,
            'line_count' => $totalLines,
            'chunk_count' => $chunkIndex
        ]); 
        Storage::disk('local')->append($this->lineConfig['chunk_dir'].$this->lineConfig['info_name'], $info);
        Cache::forever($this->lineConfig['info_cache_key'], $info); // this will only expire on new file process

        Log::info(sprintf("Processed file %s, creating %d chunks (of max %d lines) for a total of %d lines, in %s seconds", 
            $filename, 
            $chunkIndex, 
            $this->lineConfig['chunk_size'], 
            $totalLines,
            (microtime(true) - $startProcessTime)
        ));
    }

    /**
     * Read blocks of the file 
     * Explode the buffered section into auxiliary array, breaking by line termination
     * (add remainder of previous incomplete line, if necessary)
     * Write imploded auxiliary array content to active chunk and change active chunk limit reached
     * Write remainder after all the file has been read
     * 
     * @param string $filename
     * @param $fileHandle
     */
    protected function thirdApproachToReadFile($filename, $fileHandle)
    {
        $startProcessTime = microtime(true);

        $lineBuffer = '';
        $totalLines = 0;
        $linesForChunk = 0;
        $chunkIndex = 1;
        $chunkPathBase = $this->lineConfig['chunk_dir'].$this->lineConfig['chunk_name_template'];
        $chunkPath = sprintf($chunkPathBase, $chunkIndex);

        $restOfPreviousLine = '';
        $linesArray = [];
        $block = 1024*1024;
        while (!feof($fileHandle)) {// read the file
            $temp = fread($fileHandle, $block);  
            $linesRead = explode(PHP_EOL,$temp);
            $linesRead[0] = $restOfPreviousLine.$linesRead[0];
            if (!feof($fileHandle)) {
                $restOfPreviousLine = array_pop($linesRead);
            }
            foreach ($linesRead as $k => $line) {
                ++$totalLines;
                ++$linesForChunk;
                $linesArray[] = $line;
                if ($linesForChunk == $this->lineConfig['chunk_size']) {
                    Storage::disk('local')->put($chunkPath, implode(PHP_EOL, $linesArray));
                    ++$chunkIndex;
                    $linesForChunk = 0;
                    $linesArray = [];
                    $chunkPath = sprintf($chunkPathBase, $chunkIndex);
                }
            }
        }
        if (count($linesArray)) {
            Storage::disk('local')->put($chunkPath, implode('', $linesArray));
        }

        $info = json_encode([
            'is_loaded' => true,
            'line_count' => $totalLines,
            'chunk_count' => $chunkIndex
        ]); 
        Storage::disk('local')->append($this->lineConfig['chunk_dir'].$this->lineConfig['info_name'], $info);
        Cache::forever($this->lineConfig['info_cache_key'], $info); // this will only expire on new file process

        Log::info(sprintf("Processed file %s, creating %d chunks (of max %d lines) for a total of %d lines, in %s seconds", 
            $filename, 
            $chunkIndex, 
            $this->lineConfig['chunk_size'], 
            $totalLines,
            (microtime(true) - $startProcessTime)
        ));
    }

    /**
     * 
     */
    protected function logMemoryUsage()
    {
        Log::info(sprintf('Used %d MB or memory, with peak %d MB',
            round(memory_get_usage() / 1024 /1024),
            round(memory_get_peak_usage() / 1024 / 1024)
        ));
    }
}