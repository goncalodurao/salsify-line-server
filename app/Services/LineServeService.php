<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Exception;

class LineServeService
{
    protected $lineConfig = [];

    function __construct()
    {
        $this->lineConfig = config('file');
    }

    /**
     * @param int $lineNumber
     * @return string
     * @throws Exception
     */
    public function getLine($lineNumber)
    {
        /* Requested line validation
            - Is line to low (<1)
            - Is file loaded and processed
            - Is line to hight (>total lines in original file)
        */
        if ($lineNumber < 1) {
            throw new Exception('Line number invalid : too low');
        }

        // Get loaded file parameters, if miss, get from file and store in cache
        $info = Cache::get($this->lineConfig['info_cache_key'], function () {
            return (Storage::disk('local')->get($this->lineConfig['chunk_dir'].$this->lineConfig['info_name']));
        });
        
        if (!$info) {
            throw new Exception('File not loaded');
        }
        $info = json_decode($info, true);

        if ($lineNumber > $info['line_count']) {
            throw new Exception('Line number invalid : too high');
        }

        // Read the line from cache
        // If not present, retrieve from chunk and cache it
        $line = Cache::get(sprintf($this->lineConfig['line_cache_key'], $lineNumber));  

        if ($line == null) {
            $chunkIndex = $this->getChunkIndexForLineNumber($lineNumber);
            $line = $this->getLineFromChunk($chunkIndex, $this->getLineForChunk($lineNumber));
            Cache::put(sprintf($this->lineConfig['line_cache_key'], $lineNumber), $line, $this->lineConfig['line_cache_ttl']);
        }
        
        return $line;
    }

    /**
     * @param int $lineNumber
     * @return int
     */
    protected function getChunkIndexForLineNumber($lineNumber)
    {
        return (intdiv($lineNumber, $this->lineConfig['chunk_size']) + 1);
    }

    /**
     * @param int $lineNumber
     * @return int
     */
    protected function getLineForChunk($lineNumber)
    {
        return ($lineNumber % $this->lineConfig['chunk_size']);
    }

    /**
     * @param int $lineNumber
     * @return int
     * @throws Exception
     */
    protected function getLineFromChunk($chunkIndex, $lineNumber)
    {
        $chunkName = Storage::disk('local')->path($this->lineConfig['chunk_dir'].sprintf($this->lineConfig['chunk_name_template'], $chunkIndex));
        $fileHandle = @fopen($chunkName, "r");
        if (!$fileHandle) {
            throw new Exception('Unable to open chunk file');
        }

        $currentLine = 1;
        $lineBuffer = '';
        while (($lineBuffer = fgets($fileHandle)) !== false) {
            if ($currentLine == $lineNumber) {
                break;
            }
            ++$currentLine;
        }

        fclose($fileHandle);

        return $lineBuffer;
    }

}