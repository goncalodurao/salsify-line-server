<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\FileProcessService;
use Throwable;

class ProcessFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'file:process {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process a given file to be served, line by line, through the API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $file = $this->argument('file');

        try {

            $fileService = new FileProcessService();
            $fileService->process($file);
            echo "File processed and ready for API requests".PHP_EOL;
            return 0;

        } catch (Throwable $e) {
            echo "Failed to process file: ".$e->getMessage().PHP_EOL;
            return -1;
        }
    }
}
