<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\LineServeService;
use Throwable;

class LineController extends Controller
{
    /**
     * Show the profile for a given user.
     *
     * @param  int  $line
     * @return \Response
     */
    public function show($line) 
    { 
        $lineService = new LineServeService();

        try {

            $lineData = $lineService->getLine($line);

            return response($lineData, 200)
                ->header('Content-Type', 'text/plain');

        } catch (Throwable $e) {
            return response(null, 413);
        }
    }
}
