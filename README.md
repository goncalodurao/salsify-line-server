# Salsify LineServer - A (really micro)service to serve lines from a file

## For the purpose of the Take Home Exercise for Salsify recruitment process

### Candidate details && other info

- Gonçalo Durão
    - goncalo.durao@gmail.com
- Task details and information
    - Details: https://salsify.github.io/line-server.html
    - Sent by jlambert@salsify.com on the 20th Jan 2022
    - Repository created on BitBucket : https://bitbucket.org/goncalodurao/salsify-line-server


## Requirements

- Docker (https://docs.docker.com/engine/install/)
- Composer (https://getcomposer.org/download/)
    - I tried to minimize the footprint needed to run the project.
    - With Docker and Composer installed, Laravel Sail will take care of installing everything in the container and running it.


## Limitations of the current setup

#### Files outside the project folder
- While using Sail to run the server, only files inside the project directory are found.
- Trying to run the server with a file locate outside of the project folder will result in an error because the file does not exist.
- Since this issue is not directly related with the objective of the exercise I opted not to pursue a solution, and instead compromise:
    - I created a folder named files_to_process with a dummy file for quick testing purposes.
    - For visibility sake, put your test files in the files_to_process folder.
    - However, you can run the server with any file in the project folder tree.
#### Framework
- Some optimizations of the framework used would also be applied in a real solution. 


## How to run

- Jump to the project's directory
    ```sh
    cd to/where/the/project/was/cloned
    ```
- Install dependencies and run `build.sh`. This command will install all dependencies and put the environment running.
    ```sh
    ./build.sh
    ``` 
    - The first time it will take some minutes to run. 
    - Leave it running.
- Open another cli tab on the same directory and run `run.sh`. This command will process the file provided and get everything ready to serve the API requests.
    ```sh
    ./run.sh files_to_process/dummy.txt
    ```
- The endpoint requested for the exercise will be available at GET http://0.0.0.0:8084/api/lines/{line}
- `ctrl + c` will stop the server.


## How does your system work?

### Step 1 - Process the file

- When running `run.sh` with a given file, a console command (`App/Console/Commands/ProcessFile.php`) is called.
- Said command will then call the FileProcessService (`App/Services/FileProcessService.php`) `process` method:
    - Some elementary file validations are performed.
    - Informations from the previously processed file (if any) are reset:
        - File process info
        - Chunks (files with sections of the test file)
        - Cache (file process info and lines)
    - The file is read, line by line (or block by block in the third approach implemented) and the lines are written into chunk files, with the number of lines per chunk defined in the configuration/environment file.
        - Each chunk file will be named in a manner that allows for it to be found performing a calculation using the requested line number.
    - Save the results of the file processing in a persistent manner (file, for the purpose of the exercise) and also in cache.
    - Return if everything ran ok. In any other situation, an exception will be thrown and the process aborted.

    - At this point there will be a certain number of auxiliary chunk files (`(number of lines in test file / lines per chunk) + 1`), each with, at most, a number of lines defined in the lines per chunk setting.
    - There will also be a file with the results of the file processing, also available in cache.

    - In FileProcessService::process method you will find the 3 approaches I took. I left the three for the purpose of the exercise, with my notes here: 
    - Approach 1
        - This was the initial (get-something-working) approach:
            - Read line by line from the file, 
            - Append the line to the active chunk file,
            - Change active chunk when chunk limit reached.
        - This works for small files, but shows it's limitations when the size increases:
            - A small ~60KB file was processed under 1 second
            - A relatively big file ~4.8GB took 1500 seconds (~25minutes!)
            - The main issue here was the way the chunk files were being written, as I was appending each line to the file. This caused the overhead of opening + writing + closing the file to be added for each line.
            - The upside of this approach is that there is no added need for memory, as the lines are read and written at once.
        - Even though the file processing step can take a bit of time, 25 minutes per 5GB is too much. This approach is not acceptable.
    - Approach 2
        - An improvement on the first approach was to avoid opening + writing + closing the chunk file in each iteration.
        - The solution was to employ an auxiliary array to hold the lines waiting to be written:
            - Read line by line from the file, 
            - Push the line to auxiliary array,
            - Write imploded auxiliary array content to active chunk and change active chunk if limit reached,
            - Write remainder still on auxiliary array, after all the file has been read.
        - This approach saw a huge improvement:
            - The relatively big file (~4.8GB) was processed in 12~14 seconds (cold start, with no previous file processed), depending on the per-chunk-size defined (1000 and 100, respectively).
                - The memory used was ~19MB, with peak of 21MB and 42MB (100 and 1000 lines per chunk), which is understandable, given the size needed for the auxiliary array in each setting. However, this is still a decent use of memory, given the size of the test file.
    - Approach 3
        - The previous 2 approaches used a read line by line method, but during my pre-exercise investigation I had stumbled upon an example of reading by blocks of a given size and then counting the number of line endings. So I gave it a try:
            - Read blocks of the file, 
            - Explode the buffered string into auxiliary array, breaking by line termination,
                - (add remainder of previous incomplete line, if necessary),
            - Write imploded auxiliary array content to active chunk and change active chunk if limit reached,
            - Write remainder still on auxiliary array, after all the file has been read.
        - The results were similar for the 4.8GB file:
            - Processed in 13~14 seconds (again, cold start, with no previous file processed), depending on the per-chunk-size defined (1000 and 100, respectively).
            - The memory usage and peak were similar to the previous approach, as they are basically the same.

### Step 2 - Serve the line

- Making a GET request to `(http://0.0.0.0:8084 for the exercise environment)/api/lines/{line_number}`:
    ```sh
    For example:
    curl -X GET http://0.0.0.0:8084/api/lines/25
    ```
    - The route defined in `routes/api.php` will direct the request to the LineController in `App/Http/Controllers/LineController.php`.
    - The controller will then call LineServeService's `getLine` method, sending the line number as parameter.
- The `getLine` method starts by performing a basic validation of the line number parameter, using the cached info about the file.
    - If by any chance that info is not in cache, it will fetch it from the file where it is stored and cache it, before returning it.
- If the requested line number is valid:
    - Get the line data from cache, using the line number parameter,
    - If there is a cache miss:
        - Calculate the correct chunk file for the requested line number,
        - Calculate the correct line in the chunk file for the requested line number,
        - Find the line in the chunk file,
        - Cache the line data and return. 
    - Return the line data found.


## How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file?

- I performed test runs with 4 files (2.3GB, 4.8GB, 8.3GB and 24.9GB)
- In terms of memory usage and peak there will be no noticeable difference, assuming that the lines per chunk setting is the same in all file sizes.
    - The lines per chunk setting does play a role here, meaning more lines per chunk will result in less chunk files used, but with the need for more memory available for the auxiliary array, while reading the original file.
    - More lines per chunk will also mean bigger chunk files to look through on cache misses, when serving lines.
- The difference, as expected, resides on the execution time. 
    - Assuming similar contents, doubling the file size will increase execution time around 2 to 3 times:
        - 2.3GB -> 5.6 seconds (altough with slighly different content than the other files)
        - 4.8GB -> 20.4 seconds
        - 8.3GB -> 55 seconds (~2.75 times increase over 4.8GB)
        - 24.9GB -> 214.5 seconds (~10.7 time increase over 4.8GB, wich is 5 times smaller, resulting in around 2 times increase every 5GB)
        - Using the same progression, I would expect a 100GB file to use around 816 seconds to be processed.

- It should be mentioned that the average length of the lines in the file will also play a role on both the amount of memory used and the execution time.
    - A SQL file will usually use more memory and time than a similar sized HTML file.

- Sample results used for projection:
    - 2GB file (~2.3GB)
    ```sh
        creating 3460 chunks (of max 1000 lines) for a total of 3459528 lines, in 5.366519927979 seconds  
        Used 19 MB or memory, with peak 36 MB 
    ```
    - 5GB file (4.8GB)
    ```sh
        creating 4332 chunks (of max 1000 lines) for a total of 4331533 lines, in 20.390278100967 seconds  
        Used 19 MB or memory, with peak 41 MB  
    ```
    - 8GB file (8.3GB)
    ```sh
        creating 8664 chunks (of max 1000 lines) for a total of 8663066 lines, in 55.154619932175 seconds  
        Used 19 MB or memory, with peak 42 MB  
    ```
    - 25GB file (24.9GB)
    ```sh
        creating 25990 chunks (of max 1000 lines) for a total of 25989198 lines, in 214.50125408173 seconds  
        Used 19 MB or memory, with peak 42 MB
    ``` 

- A (possible) improvement for the File processing flow, for big (huge?) files would be to split the file right at the beginnig of the process, pushing each section into a pipeline and then having workers do the read-and-write-to-chunk-files. This paralelization would reduce the processing time, although at the cost of increased complexity.
    - To be noticed that I have no data regarding the execution time of the file split action, and if the paralelization using the workers would compensate the eventual overhead of said split.


## How will your system perform with 100 users? 10000 users? 1000000 users?

- Not being able to test the application with several users, the following are assumptions based on experience and knowledge of the solutions used:
- Dividing the original file in chunks, even though implies using as much (or slightly more) space as said file, allows each line search to be much faster than looking in the whole file.
    - This means that multiple users can be served without having to wait in line, or sharing search time in the same file.
    - How many users, will depend on the specs of the machine serving the application. 
- Read file operations do not lock the file, and having several chunks further spreads concurrent file accesses accross several files.
    - An extreme situation exists if several users request lines that happen to be in the same chunk, in which case the more and smaller chunks there are the less probability of this situation to ocurr exists.
- Since the line data is stored in cache (redis for the purpose of the exercise) once it is read, further requests will be automatically served without the need to search and read the chunk file again. This increases the throughput and reduces the amount of file reads.
- For very heavy usage, then the system should be scaled, with some sugestions offered:
    - If cost is no issue, the whole chunk file approach could be skipped and directly store the line data into cache during the initial file processing. This would guarantee that any request would be immediately served without the need to get and read chunk files. Disk space would be saved, eliminating file reads, but with the downside of the cost of extra memory for the cache.
        - For very big files a 'sharded cache' could be used/implemented in which case each cache instance would have just a subset of lines to serve. This would allow requests to be distributed though several cache servers instead of just one.
        - The same case can be made for replicated cache. All cache servers would have the same info, and requests would be spread through server(s) with less load.
    - Also, having this cache distributed would allow the requests to be distributed by several app instances in several servers, using a Load Balancer. This approach would increase throughput, while also increasing the availability in the event of a server malfunction or maintenance.
    - Since the content of the file is immutable, which means it only changes when a new file is processed, API caching is also a solution to reduce the number of requests that reach the endpoint, while increasing the response rate. This could be achieved using functionality of the framework used (if present) or external services, such as API caching of AWS API Gateway. 
    - Thus, a high level view of a ready for heavy load setup would be:
        - App server for File processing, that then preemptively stores the line data in the cache servers (sharded or replicated or both).
        - Cache servers with already stored line data (Key-value stores, with LineNumber-LineData).
        - App servers for Line serving, with a Load balancer for request distribution through the Line serving servers.


## What documentation, websites, papers, etc did you consult in doing this assignment?

- I had a fairly clear idea of how I wanted to tackle this exercise, with the doubt residing between reading line by line or block by block. These were the two websites that provided some insights or confirmations on my approaches:
    - https://www.ultraedit.com/company/blog/community/best-way-to-handle-large-text-files.html
    - https://stackoverflow.com/questions/13246597/how-to-read-a-large-file-line-by-line
- For consultation during the implementation:
    - https://www.php.net/manual/en/
    - https://laravel.com/docs/8.x/
- Misc documentation:
    - https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-caching.html


## What third-party libraries or other tools does the system use? How did you choose each library or framework you used?

- Laravel Framework. I used it for comodity purposes. Since it is the framework with which I'm currently working, for the purpose of the exercise I found it to be more suitable as it avoided the learning/catch up curve of other languages or frameworks.
- Laravel Sail. As mentioned by Jason Lambert in an email, the team works mainly with Ruby, making it improbable that the reviewers of the exercise would have PHP installed and running. So, Laravel Sail serves as a helper for Docker, allowing all the setup to run with just Docker installed.
    - The setup lauches two containers, one for the app itself and another for redis (used by the application to cache the processed file info and line data). 
- Redis. Laravel Sail offers out-of-the-box integration with several databases and cache systems. I wanted to use a key-value store to cache the line data after it has been read from the chunk files and also the post file process info. The options in this case were Redis and Memcached and I went for Redis because it was set up with a single configuration change.


## How long did you spend on this exercise? If you had unlimited more time to spend on this, how would you spend it and how would you prioritize each item?

- In total I invested 12~14 hours in this exercise, between:
    - Investigation and planning.
    - Setup.
    - Implementation.
    - Testing and optimization.
    - Documentation.
- If this was a real project, my backlog would be:
    - Monitoring and Data.
    - Tests.
    - Prepare the application for production (optimizations, remove unused code and features, configurations for production, etc).
    - Implement a mechanism to allow Line Serving API to continue serving requests on the 'old' file until the moment the 'new' file is completely processed.
    - Scale the Line Serving API to (above) the throughput and availability specified.
    - Reduce the processing time for large files on the File Processing service.  


## If you were to critique your code, what would you have to say about it?

- The code itself is clear and organized.
    - The exception here being the 3 methods for the 3 approaches taken, left on purpose on the code to give visibility on the progression of the solution.
    - In a final solution, or course, only the choosen approach would be present, with the surrounding code adapted where needed, and the code duplication removed.
- For the objective of the exercise the framework used has much more than needed, and optimizations could also be made in a final solution.
- Tests. I opted not to add any automatic tests, as the scope of the exercise was reduced. A complete solution should, of course, have a set of tests. 
