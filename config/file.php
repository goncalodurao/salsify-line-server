<?php

return [
    'process_approach' => env('FILE_PROCESS_APPROACH', 3),
    'chunk_size' => env('FILE_CHUNK_SIZE', 1000),
    'chunk_name_template' => env('FILE_CHUNK_NAME_TEMPLATE', "file_chunk_%d"),
    'chunk_dir' => env('FILE_CHUNK_DIR', 'app/chunks/'),
    'info_name' => env('FILE_INFO_NAME', 'file_info'),
    'info_cache_key' => env('FILE_INFO_CACHE_KEY', 'file_info'),
    'line_cache_key' => env('FILE_LINE_CACHE_KEY_TEMPLATE', 'line_%d'),
    'line_cache_ttl' => env('FILE_LINE_CACHE_TTL', 60)
];